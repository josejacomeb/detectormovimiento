TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/local/lib/ -lopencv_bgsegm -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_core -lopencv_videoio

INCLUDEPATH += $$PWD/../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../usr/local/include


unix:!macx: LIBS += -lboost_system -lboost_filesystem
