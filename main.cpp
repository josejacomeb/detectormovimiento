#include <iostream>
#include <sstream>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <opencv2/bgsegm.hpp>
#include <boost/filesystem.hpp>
#include <ctime>
using namespace cv;
using namespace std;
using namespace cv::bgsegm;
using namespace boost::filesystem;

RNG rng(0); //Generador numeros aleatorio

struct objeto {
    Scalar color;				//Color del objeto
    Point centro;               //Centro de gravedad
    vector<Point> puntos;       //Puntos anteriores
    Rect recta;
    int area;
    int cuadros;
};
vector<objeto> tracking;
time_t now;
tm *ltm;


vector<vector<cv::Point>> contours;//almacenar los puntos que da el findcontourns
vector<Vec4i> hierarchy;
vector<Point> contours_poly;

bool grabar = false, inicializargrabar = false;
VideoWriter cvgrabar;
int codec = VideoWriter::fourcc('M', 'J', 'P', 'G');  // select desired codec (must be available at runtime)

const char* params
= "{ help h         |                   | Imprimir uso }"
  "{ input          | 0                 | Número de la cámara o ruta del video }"
  "{ algo           | MOG2              | Método de sustracción (KNN, MOG2, MOG, CNT, GMG, LSBP) }"
  "{ stream         | true              | Muestra o no en una pantalla el procesamiento del video}"
  "{ dir            |                   | Directorio donde se guardan los videos}"
  "{ areaminima     | 0.04              | Proporcion area minima deteccion}"
  "{ fps            | 30                | FPS deseado a que corra}";
int main(int argc, char* argv[])
{
    CommandLineParser parser(argc, argv, params);
    parser.about( "This program shows how to use background subtraction methods provided by "
                  " OpenCV. You can process both videos and images.\n" );
    if (parser.has("help"))
    {
        //print help information
        parser.printMessage();
        return 0;
    }
    //create Background Subtractor objects
    Ptr<BackgroundSubtractor> pBackSub;
    if(parser.has("algo")){
        cout << "Cambiando el valor del algoritmo: " << parser.get<String>("algo") << endl;
    }
    if(parser.has("input")){
        cout << "Cambiando el valor de entrada: " << parser.get<String>("input") << endl;
    }
    if(parser.has("stream")){
        cout << "Cambiando el valor del stream: " << parser.get<bool>("stream") << endl;
    }
    if(parser.has("fps")){
        cout << "Cambiando el valor del fps: " << parser.get<bool>("stream") << endl;
    }
    if (parser.get<String>("algo") == "MOG2")
        pBackSub = createBackgroundSubtractorMOG2();
    else if(parser.get<String>("algo") == "CNT")
        pBackSub = createBackgroundSubtractorCNT();
    else if(parser.get<String>("algo") == "GMG")
        pBackSub = createBackgroundSubtractorGMG(20, 0.7);
    else if(parser.get<String>("algo") == "MOG")
        pBackSub = createBackgroundSubtractorMOG();
    else if(parser.get<String>("algo") == "GSOC")
        pBackSub = createBackgroundSubtractorGSOC();
    else if(parser.get<String>("algo") == "LSBP")
        pBackSub = createBackgroundSubtractorLSBP();
    else
        pBackSub = createBackgroundSubtractorKNN();

    VideoCapture capture;
    if(parser.get<String>("input") == "0" || parser.get<String>("input") == "1"){
        capture.open(parser.get<int>("input"));
    }
    else{
        capture.open(parser.get<String>("input"));
    }
    if (!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open: " << parser.get<String>("input") << endl;
        return 0;
    }
    bool stream = parser.get<bool>("stream");
    String ruta = parser.get<String>("dir");
    if(ruta != ""){
        path p(ruta);
        if(!exists(p)){
            cout << "No existe la direccion: " + ruta << endl;
            return -1;
        }
    }
    int fpsdeseados = (int)1000/parser.get<int>("fps");
    Mat frame, fgMask, procesada, gris;
    int frameCounter = 0;
    int tick = 0;
    int fps;
    std::time_t timeBegin = std::time(0);
    int areatotal = 640*480;
    float areaminima = parser.get<float>("areaminima");
    Moments momentosactual;
    const int numerocuadros = 6;
    const float offset = 0.1;
    while (true) {
        capture >> frame;
        if (frame.empty()){
            cout << "Sin imagen" << endl;
            break;
        }
        resize(frame,frame,Size(640,480));
        cvtColor(frame,gris, COLOR_BGR2GRAY);
        //medianBlur(frame, frame, 5);
        normalize(gris, gris,0,255,NORM_MINMAX,-1,Mat());
        //update the background model
        pBackSub->apply(gris, fgMask);
        threshold(fgMask,procesada,255,255, THRESH_BINARY | THRESH_OTSU);
        morphologyEx(procesada,procesada, MORPH_OPEN    ,getStructuringElement(MORPH_ELLIPSE,Size(3,3)));
        morphologyEx(procesada,procesada, MORPH_CLOSE,getStructuringElement(MORPH_ELLIPSE,Size(12,12)));
        findContours(procesada, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, cv::Point(0, 0));// encontrar contornos
        int area = 0;
        bool coincidencia = false;
        int  numerocoincidencia = 0;
        int minimomovido = 30; //30 pixeles
        Rect recta;
        Point centro;
        int distancia = 0, distanciaminima = 10000;
        for(size_t i = 0; i < contours.size(); i++){
            area = contourArea(contours.at(i));
            coincidencia = false;
            if((float)area/areatotal > areaminima){
                approxPolyDP( contours[i], contours_poly, 3, true );
                recta = boundingRect(contours_poly);
                momentosactual = moments(contours.at(i));
                centro = Point(momentosactual.m10/momentosactual.m00,momentosactual.m01/momentosactual.m00);
                distancia = 0;
                distanciaminima = 10000;
                cout << "Centro x: " << centro.x << " Centro y: " << centro.y << endl;
                for(size_t j = 0; j < tracking.size(); j++){
                    float relacion = (float) tracking.at(numerocoincidencia).area/area;
                    distancia = norm(Mat(centro), Mat(tracking.at(j).centro));
                    cout << "i: " << i << " j: " << j << " relacion: " << relacion << " Distancia: " << distancia << " Centro x: " << tracking.at(j).centro.x << " Centro y: " << tracking.at(j).centro.y << endl;
                    if(distancia < minimomovido && distancia <= distanciaminima /*&& (relacion < 1 + offset && relacion > 1 - offset  )*/){
                        coincidencia = true;
                        numerocoincidencia = j;
                        distanciaminima = distancia;
                    }
                    if(coincidencia) break;
                }
                cout << "numerocoincidencia: " << numerocoincidencia << " distanciaminima: " << distanciaminima <<  endl;
                if(coincidencia){
                    tracking.at(numerocoincidencia).puntos.push_back(tracking.at(numerocoincidencia).centro);
                    tracking.at(numerocoincidencia).centro =  centro;
                    tracking.at(numerocoincidencia).recta = recta;
                    tracking.at(numerocoincidencia).cuadros = -1;
                    tracking.at(numerocoincidencia).area = area;

                }
                else{
                    objeto temporal;
                    temporal.centro = centro;
                    temporal.recta = recta;
                    temporal.cuadros = -1;
                    temporal.area = area;
                    temporal.color = Scalar(rng.uniform(0,255), rng.uniform(0,255), rng.uniform(0,255) );
                    tracking.push_back(temporal);
                }
            }

        }
        for(size_t i = 0; i < tracking.size(); i ++){
            if(tracking.at(i).cuadros > numerocuadros) tracking.erase(tracking.begin() + i);
        }

        frameCounter++;

        std::time_t timeNow = std::time(0) - timeBegin;

        if (timeNow - tick >= 1)
        {
            tick++;
            fps = frameCounter;
            frameCounter = 0;
        }
        if(stream){
            for(size_t i = 0; i < tracking.size(); i ++){
                putText(frame, "Persona " + to_string(i+1), Point(tracking.at(i).recta.x, tracking.at(i).recta.y + 5 ),
                        FONT_HERSHEY_SIMPLEX, 1, tracking.at(i).color,4);
                ellipse(frame, tracking.at(i).centro, Size(20,10),
                        0, 0 , 0, tracking.at(i).color);
                float tamaño = 8;
                for(size_t j = 0; j < tracking.at(i).puntos.size(); j++){
                    circle(frame, tracking.at(i).puntos.at(j),tamaño, tracking.at(i).color/2, -1);
                    tamaño/= 1.003;
                }
            }
            rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                      cv::Scalar(255,255,255), -1);
            string datofps = "FPS: " + to_string(fps);
            putText(frame, datofps.c_str(), cv::Point(15, 15),
                    FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));
            //show the current frame and the fg masks
            imshow("Frame", frame);
            imshow("FG Mask", fgMask);
            imshow("Procesada", procesada);
            //get the input from the keyboard
            int keyboard = waitKey(fpsdeseados);
            if (keyboard == 'q' || keyboard == 27)
                break;
        }
        else{
            cout << "FPS: " <<fps << endl;
        }
        for(size_t i = 0; i < tracking.size(); i ++){
            tracking.at(i).cuadros +=1;
        }
        if(tracking.size() == 0){
            grabar = false;
            inicializargrabar = false;
            cvgrabar.release();
        }
        else{
            if(grabar){
                if(inicializargrabar) cvgrabar << frame;
                else{
                    now = time(0);
                    ltm = localtime(&now);
                    cvgrabar.open(ruta + to_string(1900+ ltm->tm_year) + "-" +
                                to_string(1+ ltm->tm_mon) + "-" + to_string(ltm->tm_mday) +
                                "-" + to_string(1+ ltm->tm_hour) +"-" + to_string(1+ ltm->tm_min)
                                + "-" + to_string(1+ ltm->tm_sec) + ".avi", codec, fps, frame.size());
                    inicializargrabar = true;
                }
            }
            else{
                grabar = true;
            }
        }

    }
    cvgrabar.release();
    capture.release();
    return 0;
}
